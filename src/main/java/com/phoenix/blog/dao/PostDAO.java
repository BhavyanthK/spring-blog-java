package com.phoenix.blog.dao;

import com.phoenix.blog.model.Post;
import com.phoenix.blog.model.Category;
import com.phoenix.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostDAO extends JpaRepository<Post, Long> {
    List<Post> findAllByCategory(Category category);

    List<Post> findByUser(User user);
}
