package com.phoenix.blog.dto;

import com.phoenix.blog.model.VoteType;

public class VoteDto {
    private VoteType voteType;
    private Long postId;
    
	public VoteType getVoteType() {
		return voteType;
	}
	public void setVoteType(VoteType voteType) {
		this.voteType = voteType;
	}
	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
    
}
