package com.phoenix.blog.exceptions;

public class SpringException extends RuntimeException {
    
	private static final long serialVersionUID = 1L;

	public SpringException(String exMessage, Exception exception) {
        super(exMessage, exception);
    }

    public SpringException(String exMessage) {
        super(exMessage);
    }
}
