package com.phoenix.blog.service;

import com.phoenix.blog.exceptions.SpringException;
import com.phoenix.blog.mapper.CategoryMapper;
import com.phoenix.blog.dao.CategoryDAO;
import com.phoenix.blog.dto.CategoryDto;
import com.phoenix.blog.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class CategoryService {

    private  CategoryDAO categoryDao;
    private  CategoryMapper categoryMapper;

    @Transactional
    public CategoryDto save(CategoryDto categoryDto) {
        Category save = categoryDao.save(categoryMapper.mapDtoToCategory(categoryDto));
        categoryDto.setId(save.getId());
        return categoryDto;
    }

    @Transactional(readOnly = true)
    public List<CategoryDto> getAll() {
        return categoryDao.findAll()
                .stream()
                .map(categoryMapper::mapCategoryToDto)
                .collect(toList());
    }

    public CategoryDto getCategory(Long id) {
        Category category = categoryDao.findById(id)
                .orElseThrow(() -> new SpringException("No category found with ID - " + id));
        return categoryMapper.mapCategoryToDto(category);
    }
}
