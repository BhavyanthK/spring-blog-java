package com.phoenix.blog.service;

import com.phoenix.blog.exceptions.PostNotFoundException;
import com.phoenix.blog.dao.CommentDAO;
import com.phoenix.blog.dao.PostDAO;
import com.phoenix.blog.dao.UserDAO;
import com.phoenix.blog.dto.CommentsDto;
import com.phoenix.blog.mapper.CommentMapper;
import com.phoenix.blog.model.Comment;
import com.phoenix.blog.model.NotificationEmail;
import com.phoenix.blog.model.Post;
import com.phoenix.blog.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class CommentService {
    private static final String POST_URL = "";
    private  PostDAO postRepository;
    private  UserDAO userRepository;
    private  AuthService authService;
    private  CommentMapper commentMapper;
    private  CommentDAO commentRepository;
    private  MailContentBuilder mailContentBuilder;
    private  MailService mailService;

    public void save(CommentsDto commentsDto) {
        Post post = postRepository.findById(commentsDto.getPostId())
                .orElseThrow(() -> new PostNotFoundException(commentsDto.getPostId().toString()));
        Comment comment = commentMapper.map(commentsDto, post, authService.getCurrentUser());
        commentRepository.save(comment);

        String message = mailContentBuilder.build(authService.getCurrentUser() + " posted a comment on your post." + POST_URL);
        sendCommentNotification(message, post.getUser());
    }

    private void sendCommentNotification(String message, User user) {
        mailService.sendMail(new NotificationEmail(user.getUsername() + " Commented on your post", user.getEmail(), message));
    }

    public List<CommentsDto> getAllCommentsForPost(Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId.toString()));
        return commentRepository.findByPost(post)
                .stream()
                .map(commentMapper::mapToDto).collect(toList());
    }

    public List<CommentsDto> getAllCommentsForUser(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException(userName));
        return commentRepository.findAllByUser(user)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(toList());
    }
}
