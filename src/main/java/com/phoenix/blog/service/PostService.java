package com.phoenix.blog.service;

import com.phoenix.blog.exceptions.PostNotFoundException;
import com.phoenix.blog.exceptions.CategoryNotFoundException;
import com.phoenix.blog.mapper.PostMapper;
import com.phoenix.blog.dao.PostDAO;
import com.phoenix.blog.dao.CategoryDAO;
import com.phoenix.blog.dao.UserDAO;
import com.phoenix.blog.dto.PostRequest;
import com.phoenix.blog.dto.PostResponse;
import com.phoenix.blog.model.Post;
import com.phoenix.blog.model.Category;
import com.phoenix.blog.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class PostService {

    private  PostDAO postRepository;
    private  CategoryDAO categoryDao;
    private  UserDAO userRepository;
    private  AuthService authService;
    private  PostMapper postMapper;

    public void save(PostRequest postRequest) {
        Category category = categoryDao.findByName(postRequest.getcategoryName())
                .orElseThrow(() -> new CategoryNotFoundException(postRequest.getcategoryName()));
        postRepository.save(postMapper.map(postRequest, category, authService.getCurrentUser()));
    }

    @Transactional(readOnly = true)
    public PostResponse getPost(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException(id.toString()));
        return postMapper.mapToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAllPosts() {
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByCategory(Long categoryId) {
        Category category = categoryDao.findById(categoryId)
                .orElseThrow(() -> new CategoryNotFoundException(categoryId.toString()));
        List<Post> posts = postRepository.findAllByCategory(category);
        return posts.stream().map(postMapper::mapToDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return postRepository.findByUser(user)
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }
}
